use std::process::{exit, Command};
use std::thread::{sleep, spawn};
/// This small tool requires three inputs:
///
///  * A timeout
///  * A command to execute immediately
///  * A command to execute in case the previous command did not finish in time
///
use std::time::Duration;

use structopt::StructOpt;

use humantime::parse_duration;

fn string_to_command<T: AsRef<str>>(s: T) -> std::io::Result<(String, Vec<String>)> {
    let mut iter = s.as_ref().split_whitespace();

    if let Some(command) = iter.next() {
        let args = iter.map(|x| x.to_string()).collect();

        Ok((command.to_string(), args))
    } else {
        // I won't create a new a Error type for this single occurrence
        Err(std::io::Error::from(std::io::ErrorKind::InvalidInput))
    }
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "timelock",
    about = "A stupid, small tool that fires a second command if a first one didn't finish in time"
)]
struct Timelock {
    /// The timeout for the initial command
    #[structopt(
        long = "timeout",
        short = "t",
        parse(try_from_str = "parse_duration")
    )]
    timeout: Duration,
    /// The command to be run initially
    #[structopt(
        long = "command",
        short = "c",
        parse(try_from_str = "string_to_command")
    )]
    command: (String, Vec<String>),
    /// The alternative command to be run
    #[structopt(
        long = "alternative",
        short = "a",
        parse(try_from_str = "string_to_command")
    )]
    alternative: (String, Vec<String>),
    /// Print debug information
    #[structopt(short = "d")]
    debug: bool,
}

fn main() {
    let timelock = Timelock::from_args();

    let _debug = timelock.debug;

    if _debug {
        eprintln!("{:?}", timelock);
    }
    // Create initial command
    let mut command = Command::new(timelock.command.0.as_str());
    command.args(&timelock.command.1);

    // Create alternative
    let mut alternative = Command::new(timelock.alternative.0.as_str());
    alternative.args(&timelock.alternative.1);

    if _debug {
        eprintln!("Spawning command thread");
    }
    // Spawn the initial command as a subprocess
    spawn(move || {
        if _debug {
            eprintln!("Spawning command subprocess");
        }
        let _ = command
            .spawn()
            .expect("Command could not be executed")
            .wait();

        if _debug {
            eprintln!("Command finished. Exiting.");
        }
        // this thread will terminate the program, if command finishes before timeout
        exit(0);
    });

    if _debug {
        eprintln!("Sleeping for {:?}", timelock.timeout);
    }

    // Sleep for specified duration
    sleep(timelock.timeout);

    // Execute alternative
    if _debug {
        eprintln!("Spawning alternative command");
    }
    let _ = alternative
        .spawn()
        .expect("Alternative could not be executed")
        .wait();
    if _debug {
        eprintln!("Alternative finished. Exiting.");
    }
    exit(1);
}
