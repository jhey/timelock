# Timelock

This is a simple tool that executes a second command if a first one does not
terminate in time. I use it as a tool to lock my screen. If I do not return in
time, it activates hibernation mode and thus my full disk encryption.

## Why is this not a bash tool?

Actually, the basic functionality would be pretty trivial to write in bash.
I do not like argument parsing in bash though and I wanted this tool to accept 
time suffixes at the timeout parameter.
Additionally, I wanted to try out the 
[StructOpt](https://crates.io/crates/structopt) library.
For parsing the timeout parameter, I use 
[humantime](https://crates.io/crates/humantime).


## Uargh, you are spawning a thread!

Yes I am. Maybe I am going to patch it, once I can use async/await on stable.

# Usage
```
# timelock -h

timelock 0.1.0
Jens Heyens <jens.heyens@ewetel.net>
A stupid, small tool that fires a second command if a first one didn't finish in time

USAGE:
    timelock [FLAGS] --alternative <alternative> --command <command> --timeout <timeout>

FLAGS:
    -d               Print debug information
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --alternative <alternative>    The alternative command to be run
    -c, --command <command>            The command to be run initially
    -t, --timeout <timeout>            The timeout for the initial command
```
Command and alternative are a single string (e.g. "i3lock -n").
Timeout can be everything [humantime](https://crates.io/crates/humantime) is 
able to parse into a Duration, e.g. `5m`, `2 hours 20 seconds`, `5s` etc.


# Build

```
cargo build --release
```

# Example

I am using i3wm with arch linux. If I want to lock my screen and I expect to be
back within 10 minutes, I use:
```
timelock -t 10m -c "i3lock -n" -a "systemctl hibernate"
```

This will lock my screen. If the screen is still locked after 10 minutes, it
will go into hibernation, thus enabling my full disk encryption.

# Limitations

The command string parsing is stupid. You can't provide whitespaces in a command's argument since it would be interpreted as multiple arguments.
Additionally, you need to make sure that the command provided does not fork immediately and exit. This is the reason for the `-n` switch to i3lock in the example.
